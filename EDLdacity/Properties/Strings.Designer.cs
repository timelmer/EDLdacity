﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace EDLdacity.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Strings {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Strings() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("EDLdacity.Properties.Strings", typeof(Strings).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Enter note: .
        /// </summary>
        internal static string enterNote {
            get {
                return ResourceManager.GetString("enterNote", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Failed to parse label.
        /// </summary>
        internal static string failParse {
            get {
                return ResourceManager.GetString("failParse", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to File exists. Overwrite? [Y/N] .
        /// </summary>
        internal static string fileExists {
            get {
                return ResourceManager.GetString("fileExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Finished. Press any key to exit..
        /// </summary>
        internal static string finished {
            get {
                return ResourceManager.GetString("finished", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Converts an Audacity label track to EDL markers.
        ///
        ///EDLdacity [/F] [/N] [/T] [/V] source.txt
        ///
        ///	source	Specifies a text file with Audacity labels. Output will be placed in &lt;source&gt;.edl
        ///
        ///	/F:n	Set framerate to n (default is 24)
        ///	/N	Set notes while parsing
        ///	/T:n	Set target track to n (default is 1)
        ///	/V	Show verbose output.
        /// </summary>
        internal static string help {
            get {
                return ResourceManager.GetString("help", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid file.
        /// </summary>
        internal static string invalidFile {
            get {
                return ResourceManager.GetString("invalidFile", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid framerate.
        /// </summary>
        internal static string invalidFramerate {
            get {
                return ResourceManager.GetString("invalidFramerate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid number of arguments.
        /// </summary>
        internal static string invalidNumArg {
            get {
                return ResourceManager.GetString("invalidNumArg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid track.
        /// </summary>
        internal static string invalidTrack {
            get {
                return ResourceManager.GetString("invalidTrack", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parsed &quot;{0}&quot;: {1} -&gt; {2}.
        /// </summary>
        internal static string parsed {
            get {
                return ResourceManager.GetString("parsed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Parsed &quot;{0}&quot;: {1} -&gt; {2} from &quot;{3}&quot;.
        /// </summary>
        internal static string parsedFrom {
            get {
                return ResourceManager.GetString("parsedFrom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to First line empty, skipping.
        /// </summary>
        internal static string skipFirstLine {
            get {
                return ResourceManager.GetString("skipFirstLine", resourceCulture);
            }
        }
    }
}
