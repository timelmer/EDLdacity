﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using static EDLdacity.Properties.Strings;

namespace EDLdacity
{
    /// <summary>
    ///     Using exit codes as described at https://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx
    /// </summary>
    class Program
    {
        private static readonly int E_FILE_NFOUND   = 2;        // The system cannot find the file specified.
        private static readonly int E_ACC_DENY      = 5;        // Access is denied.
        private static readonly int E_PATH_INVAL    = 161;      // The specified path is invalid.
        private static readonly int E_DIR_INVAL     = 267;      // The directory name is invalid.
        private static readonly int E_IO_FAIL       = 1117;     // The request could not be performed because of an I/O device error.
        private static readonly int E_ARG_INVAL     = 10022;    // An invalid argument was supplied.

        private static decimal framerate = 24;
        private static int targetTrack = 1;
        private static string[] source;
        private static string dest;
        private static List<Label> labels = new List<Label>();
        private static bool note, verbose;

        static void Main(string[] args)
        {
            // Check arg count
            if (args.Count() < 1 || args.Count() > 5)
                ExitError(invalidNumArg, E_ARG_INVAL, true);

            // Catch help option
            if (args.FirstOrDefault(a => a.Contains("/?")) != null)
            {
                Console.WriteLine(help);
                Environment.Exit(0);
            }

            // Try parsing framerate
            string argFramerate = args.FirstOrDefault(a => a.ToUpper().Contains("/F:"));
            if (argFramerate != default(string) && !decimal.TryParse(argFramerate.Trim().ToUpper().Substring(3), out framerate))
                ExitError(invalidFramerate, E_ARG_INVAL, true);

            // Check Note
            note = args.FirstOrDefault(a => a.ToUpper().Contains("/N")) != default(string);

            // Try parsing track
            string argTrack = args.FirstOrDefault(a => a.ToUpper().Contains("/T:"));
            if (argTrack != default(string) && !int.TryParse(argTrack.Trim().ToUpper().Substring(3), out targetTrack))
                ExitError(invalidTrack, E_ARG_INVAL, true);

            // Check Verbose
            verbose = args.FirstOrDefault(a => a.ToUpper().Contains("/V")) != default(string);

            // Check for output conflict
            dest = String.Format("{0}.edl", Path.GetFileNameWithoutExtension(args.Last().Trim()));
            if (File.Exists(dest))
            {
                bool confirm = false;
                do
                {
                    char response;
                    Console.Write(fileExists);
                    response = Console.ReadKey().KeyChar;
                    Console.WriteLine();
                    if (response == 'Y' || response == 'y')
                        confirm = true;
                    else if (response == 'N' || response == 'n')
                        Environment.Exit(0);
                }
                while (!confirm);
            }

            // Try loading file
            if (!File.Exists(args.Last().Trim()))
                ExitError(invalidFile, E_ARG_INVAL, true);
            try
            {
                source = File.ReadAllLines(args.Last().Trim());
            }
            catch (ArgumentException e)
            {
                ExitError(e.Message, E_ARG_INVAL, true);
            }
            catch (PathTooLongException e)
            {
                ExitError(e.Message, E_PATH_INVAL);
            }
            catch (DirectoryNotFoundException e)
            {
                ExitError(e.Message, E_DIR_INVAL);
            }
            catch (FileNotFoundException e)
            {
                ExitError(e.Message, E_FILE_NFOUND);
            }
            catch (IOException e)
            {
                ExitError(e.Message, E_IO_FAIL);
            }
            catch (UnauthorizedAccessException e)
            {
                ExitError(e.Message, E_ACC_DENY);
            }
            catch (NotSupportedException e)
            {
                ExitError(e.Message, E_PATH_INVAL);
            }
            catch (System.Security.SecurityException e)
            {
                ExitError(e.Message, E_ACC_DENY);
            }
            catch (Exception e)
            {
                ExitError(e.Message);
            }

            // Parse file
            foreach (string line in source)
            {
                string[] cols = line.Split(new char[] { '\t' });
                Label label = new Label();

                decimal inTimeSec, outTimeSec;

                if (!decimal.TryParse(cols[0], out inTimeSec) || 
                    !decimal.TryParse(cols[1], out outTimeSec))
                {
                    Warn(String.Format("{0}: {1}", failParse, line));
                    continue;
                }

                // Dump first line
                if (inTimeSec == outTimeSec && outTimeSec == 0)
                {
                    Info(skipFirstLine);
                    continue;
                }

                label.Duration = (int)((outTimeSec - inTimeSec) * framerate);

                // Convert seconds to timecode
                int[] inTime = new int[4], outTime = new int[4];
                inTime[0] = (int)(inTimeSec / 3600);
                outTime[0] = (int)(outTimeSec / 3600);
                inTimeSec -= inTime[0] * 3600;
                outTimeSec -= outTime[0] * 3600;
                inTime[1] = (int)(inTimeSec / 60);
                outTime[1] = (int)(outTimeSec / 60);
                inTimeSec -= inTime[1] * 60;
                outTimeSec -= outTime[1] * 60;
                inTime[2] = (int)inTimeSec;
                outTime[2] = (int)outTimeSec;
                inTimeSec -= (int)inTimeSec;
                outTimeSec -= (int)outTimeSec;
                inTime[3] = (int)(inTimeSec * framerate);
                outTime[3] = (int)(outTimeSec * framerate);

                label.In = inTime;
                label.Out = outTime;

                label.Title = cols[2];

                Info(verbose ? String.Format(parsedFrom, label.Title, label.In, label.Out, line) : String.Format(parsed, label.Title, label.FormattedIn, label.FormattedOut));
                if (note)
                {
                    Console.Write(enterNote);
                    label.Note = Console.ReadLine();
                }
                labels.Add(label);
            }

            // Write output
            int index = 1;
            string output = "TITLE: Markers\r\r\nFCM: NON-DROP FRAME\r\r\n\r\r\n";
            foreach (Label label in labels)
            {
                output += String.Format("{0:000}  {1:000}      V     C        {2} {3} {2} {3}  \r\r\n", index++, targetTrack, label.FormattedIn, label.FormattedOut);
                output += String.Format("{0} |C:ResolveColorBlue |M:{1} |D:{2}\r\r\n\r\r\n", label.Note, label.Title, label.Duration);
            }

            try
            {
                File.WriteAllText(dest, output);
            }
            catch (ArgumentException e)
            {
                ExitError(e.Message, E_ARG_INVAL, true);
            }
            catch (PathTooLongException e)
            {
                ExitError(e.Message, E_PATH_INVAL);
            }
            catch (DirectoryNotFoundException e)
            {
                ExitError(e.Message, E_DIR_INVAL);
            }
            catch (FileNotFoundException e)
            {
                ExitError(e.Message, E_FILE_NFOUND);
            }
            catch (IOException e)
            {
                ExitError(e.Message, E_IO_FAIL);
            }
            catch (UnauthorizedAccessException e)
            {
                ExitError(e.Message, E_ACC_DENY);
            }
            catch (NotSupportedException e)
            {
                ExitError(e.Message, E_PATH_INVAL);
            }
            catch (System.Security.SecurityException e)
            {
                ExitError(e.Message, E_ACC_DENY);
            }
            catch (Exception e)
            {
                ExitError(e.Message);
            }

            Info(finished);
            Console.ReadKey();
        }

        /// <summary>
        ///     Display an error and exit
        /// </summary>
        /// <param name="error">Error to display</param>
        /// <param name="exitCode">Exit code (default 1)</param>
        /// <param name="showHelp">Show help after error (default false)</param>
        private static void ExitError(string error, int exitCode = 1, bool showHelp = false)
        {
            Console.WriteLine(showHelp ? 
                String.Format("ERR: {0}\r\n{1}", error, help) : 
                String.Format("ERR: {0}", error));
            Console.ReadKey();
            Environment.Exit(exitCode);
        }

        /// <summary>
        ///     Display a warning
        /// </summary>
        /// <param name="warning">Warning to display</param>
        private static void Warn(string warning) => Console.WriteLine(String.Format("WARN: {0}", warning));

        private static void Info(string message) => Console.WriteLine(String.Format("INFO: {0}", message));
    }
}
