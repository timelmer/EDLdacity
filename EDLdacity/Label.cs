﻿namespace EDLdacity
{
    class Label
    {
        public string Title;
        public string Note = "";

        /// <summary>
        ///     { H, M, S, F }
        /// </summary>
        public int[] In;

        /// <summary>
        ///     { H, M, S, F }
        /// </summary>
        public int[] Out;
        public int Duration;
        
        /// <summary>
        ///     HH:MM:SS:FF
        /// </summary>
        public string FormattedIn => Format(ref In);

        /// <summary>
        ///     HH:MM:SS:FF
        /// </summary>
        public string FormattedOut => Format(ref Out);

        private string Format(ref int[] ints)
        {
            string o = "";
            for (int i = 0; i < ints.Length; i++)
                o += string.Format("{0:00}{1}", ints[i], i == ints.Length - 1 ? "" : ":");
            return o;
        }
    }
}
